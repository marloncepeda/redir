<?php
/**
 * Access from index.php:
 */


class Default_Controller extends ZP_Load {
	
	public function __construct() {
		$this->app("default");	
		$this->Templates = $this->core("Templates");
		$this->Templates->theme();
		$this->default_Model = $this->model("Default_Model");
	}
	
	public function index() { 

		$this->login();
	}

	public function salir() {	
		unsetSessions($URL); //Elimina sessiones
		$this->login(); //redirige a login
	}

	public function login(){
		if(SESSION("usuario")==""){	
			if(POST("entrar")){ 
				$data = $this->default_Model->login(POST("usuario"),POST("clave"));
				if($data == FALSE){
					echo "<script> alert('su usuario no esta en la base de datos')</script>";
					$vars["view"] = $this->view("login",TRUE);
					$this->render("content",$vars);
				}else{

					SESSION("id", $data[0]["id"]);
					SESSION("usuario", $data[0]["usuario"]);
					SESSION("clave", $data[0]["clave"]);

					echo "<script> alert('entraste al sistema')</script>";
					$vars["view"] = $this->view("home",TRUE);
					$this->render("content",$vars);
				}
			}else{
				$vars["view"] = $this->view("login",TRUE);
				$this->render("content",$vars);
			}
		}else{
			echo "<script> alert('Ya estas adentro')</script>";
			$vars["view"] = $this->view("home",TRUE);
			$this->render("content",$vars);
		}
	}

}
